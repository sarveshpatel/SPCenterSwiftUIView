// swift-tools-version: 5.8
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "SPCenterSwiftUIView",
    platforms: [
        .iOS(.v15),
    ],
    products: [
        .library(
            name: "SPCenterSwiftUIView",
            targets: ["SPCenterSwiftUIView"]),
    ],
    
    dependencies: [],
    targets: [
        .target(
            name: "SPCenterSwiftUIView",
            dependencies: [])
       
    ]
)
