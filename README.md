# SPCenterSwiftUIView


## Installation

To install SPCenterSwiftUIView package, import `https://gitlab.com/sarveshpatel/SPCenterSwiftUIView` in SPM

## Usage example

```swift

import SwiftUI
struct ContentView: View {
    var body: some View {
        Text("Hello SwiftUI!")
            .centerHorizontally()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

```
